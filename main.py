#!/usr/bin/env python

import cv2 as cv
import rospy
import time
import sensor 
import aruco
import navControls
import computeControl
import equations as eq
import numpy as np

class Main(object):
	def __init__(self):
		self.dt=0.1

		self.data = sensor.SensorData()
		self.detect = aruco.Aruco("DICT_5X5_100")
		self.nav = navControls.Navigation()
		self.con = computeControl.Control(self.dt)

		self.prevTime = 0
		self.data.init_node()
		rospy.loginfo("emmi koddayiz")
		self.is_steady_state = False
		self.is_landed = False

		self.MARKER_REMINDER_DICT = {}
		self.control=0

		
		self.r = rospy.Rate(10)

	def loop(self):
		while not rospy.is_shutdown():
			
			img=self.data.get_image()
			self.detect.look_at_the_marker(img)
			frame = self.detect.get_frame()
			self.r.sleep()
			yaw = self.nav.get_yaw()
			alt=self.nav.altitude()

			
			if self.detect.is_detected():
				u = self.con.image_error_control(self.dt,self.detect.markerCenter,
					self.detect.frameCenter, self.detect.Area, 1.3,self.detect.frameMarkerYawAngle())
				self.control = np.around(np.dot(eq.rotationMatrix(yaw),u.T),3)
			if bool(self.MARKER_REMINDER_DICT):
				u = self.con.image_error_control(self.dt,self.MARKER_REMINDER_DICT["pose"],
					self.detect.frameCenter, self.MARKER_REMINDER_DICT["area"], 1.3,self.MARKER_REMINDER_DICT["yaw"])
				self.control = np.around(np.dot(eq.rotationMatrix(yaw),u.T),3)

			print("")
			print("")
			print("---------- INFO ----------")
			print("frame center: {}".format(self.detect.frameCenter))
			print("marker center: {}".format(self.detect.markerCenter))
			print("marker surface area: {}".format(self.detect.Area))
			print("marker yaw relative to frame: {}".format(self.detect.frameMarkerYawAngle()))
			print("is marker detected: {}".format(self.detect.is_marker_detected))
			print("yaw: {}".format(yaw))
			print("altitude: {}".format(alt))
			print("distance: {}".format(self.detect.dist_m))
			print("saved marker pose: {}".format(self.MARKER_REMINDER_DICT))
			print("is marker position in steady state: {}".format(self.is_steady_state))
			print("---------- COTROL COMMAND ----------")
			print("control command: {}".format(self.control))
			print("--------------------")
			print("")
			print("")

			# if able to lan: land
			# elif marker still detectign: go to steady state
			# elif marker seen once than disapper for a moment: use dictionaried marker pose to go steady state 
			# else never seen a marker: search for it
			
			if (self.is_steady_state  or alt >= -1) and self.MARKER_REMINDER_DICT["yaw"]<0.1:
				print("CONDITION I")
				

				if alt >= -0.7:
					print("CONDITION I.I")
					self.nav.set_speed_vx_vy_vz_wpsi(0,0,.1,0)
					#self.MARKER_REMINDER_DICT={"pose":self.detect.markerCenter,"yaw":self.detect.frameMarkerYawAngle(),
					#						"area":self.detect.Area}

					if alt >=-0.5:
						print("CONDITION I.I.I")
						
						self.nav.land()
						
						self.is_landed = True
				else:
					print("CONDITION I.II")
					self.nav.set_speed_vx_vy_vz_wpsi(-eq.speed_bound(self.control[0]),eq.speed_bound(self.control[1]), .1,eq.speed_bound(self.control[3]))
					#self.MARKER_REMINDER_DICT={"pose":self.detect.markerCenter,"yaw":self.detect.frameMarkerYawAngle(),
					#							"area":self.detect.Area}

			elif self.detect.is_detected():
				print("CONDITION II")
				self.nav.set_speed_vx_vy_vz_wpsi(-eq.speed_bound(self.control[0]),eq.speed_bound(self.control[1]),
												(eq.speed_bound(self.control[2])),eq.speed_bound(self.control[3]))
				self.MARKER_REMINDER_DICT={"pose":self.detect.markerCenter,"yaw":self.detect.frameMarkerYawAngle(),
											"area":self.detect.Area}

			elif bool(self.MARKER_REMINDER_DICT):
				print("CONDITION III")

				self.nav.set_speed_vx_vy_vz_wpsi(-eq.speed_bound(self.control[0]),eq.speed_bound(self.control[1]),
												(eq.speed_bound(self.control[2])),eq.speed_bound(self.control[3]))
			else:
				print("CONDITION IV")
				rospy.loginfo("ArUCo is not detected !")
				rospy.loginfo("Searching for ArUCo")
				self.nav.set_speed_vx_vy_vz_wpsi(0.1,0,0,0)

			if self.detect.is_detected():
				self.is_steady_state= eq.is_steady_state2(self.detect.dist_m,0.15,self.detect.frameMarkerYawAngle(),0.03)
			if bool(self.MARKER_REMINDER_DICT):
				self.is_steady_state= eq.is_steady_state2(eq.distance(self.MARKER_REMINDER_DICT["pose"],self.detect.frameCenter),
																	0.15,self.MARKER_REMINDER_DICT["yaw"],0.03)

			
			cv.imshow("Frame", frame)
			self.r.sleep()

			currentTime = time.time()
			self.dt=currentTime - self.prevTime
			self.prevTime = currentTime
			
			if cv.waitKey(1) & 0xFF == ord('q'):
				break
			elif self.is_landed:
				print("!!! Quadrotor Landed on the Marker !!!")
				break


def main():
	file = Main()

	file.nav.takeoff(2)
	if file.nav.altitude()>-1.7:
		while file.nav.altitude()>-1.7:
			file.nav.set_speed_vx_vy_vz_wpsi(0,0,-.2,0)
			print("Waiting to reach altitude: {}".format(file.nav.altitude()))
			if file.nav.altitude()<-1.7:
				break

	file.loop()
	rospy.sleep(10)
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")

if __name__ == '__main__':
	main()