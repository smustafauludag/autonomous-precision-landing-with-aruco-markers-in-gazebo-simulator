#!/usr/bin/env python

import numpy as np
import equations as eq
import sensor
from time import sleep



class Control(object):
	def __init__(self,dt):
		self.dt = dt

		self.Kp = 0.07
		self.Kd = 0.03
		self.Ki = 0.007
		self.PID = np.array([self.Kp,self.Kd,self.Ki])
		self.error_prev = np.zeros(4)
		
		self.g = 0.00005
		self.h = 0.003
		self.k = 0.001
		self.GHK=np.array([self.g,self.h,self.k])

		self.initialGuessPose = np.array([413,210]) #(x,y)
		self.initialGuessVelocity = np.array([1,1])*10**-9 #(vx,vy)
		self.initialGuessAccel = np.array([0,0]) #(ax,ay)
		self.initialGuessState =np.array([self.initialGuessPose,self.initialGuessVelocity,self.initialGuessAccel])
		self.prevState = eq.stateExtrapolation2D(self.initialGuessState,self.dt)
		self.estimatedStatePose = np.zeros(2)
		self.nextStatePose = np.zeros(2)



	def image_error_control(self,dt,p1,p2,A,desiredZ,yaw):
		'''
		:param dt:	time differet
		:param p1:	DESIRED marker center (x,y)
		:param p2:	MEASURED frame center
		:param A:	MEASURED area of the marker
		:param desiredZ: DESIRED height from the marker
		:param yaw:	angle between marker and the frame
		:returns: 	error matrix to compute control commands
		'''
		
		markerCenter = np.array(p1)
		frameCenter = np.array(p2)

		'''
		#START: MARKER CENTER PREDICTION

		#---------- NOT WORKING PROPERLY ----------#

		measuredPose = markerCenter
		
		estimated_state = eq.stateUpdate2D(self.prevState,measuredPose,self.GHK,dt)
		print("estimated_state_x: "+str(int(estimated_state[0][0])))

		next_state = eq.stateExtrapolation2D(estimated_state,dt)
		print("predicted_state: "+str(np.around(next_state[0],2)))
		
		self.estimatedStatePose = estimated_state[0]
		self.nextStatePose = next_state[0]
		self.prevState = next_state

		#END: MARKER CENTER PREDICTION
		'''

		desired_y_x_z_psi = np.array([markerCenter[1],markerCenter[0],desiredZ,yaw])
		measured_y_x_z_psi = np.array([frameCenter[1],frameCenter[0],-34.996/np.sqrt(A),0])
		print("desired pose: {}".format(desired_y_x_z_psi))
		print("measured pose: {}".format(measured_y_x_z_psi))

		error_in_pixel = desired_y_x_z_psi - measured_y_x_z_psi
		error_in_meter = np.dot(eq.pixel2meterMatrix(A),error_in_pixel.T)
		error_d = (error_in_meter-self.error_prev)/dt
		error_i = error_in_meter + self.error_prev
		error_prev=error_in_meter

		error_matrix = np.vstack((error_in_meter,error_d,error_i)).T
		image_error_control = np.dot(error_matrix,self.PID.T) # [Ux, Uy, Uz, Upsi]

		return image_error_control


